# Commit messages
Exploring the relation between design and commit messages.

# Building
The notebook at the root of the project contains the source code. We use Python 3.7 with the librairies _sklearn_, _pandas_, and _nltk_.

You will also need to place the Software Engineering Word Embedding model in the folder _/model_ at the root of the project. The model can be downloaded from https://github.com/vefstathiou/SO_word2vec (1.5GB).

# Running
You will need to run a Jupyter service in order to work with the notebook.